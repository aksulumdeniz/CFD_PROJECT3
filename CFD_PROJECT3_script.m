clear all; 
clc;
global node_number; % Number of nodes
global delta_x;     % Grid resolution
global gamma;       % Adiabatic gas index 
gamma = 1.4;        % Ideal gas
delta_x = input('Please enter grid resolution.\n');
order = input('Please enter the order for MUSCL interpolation. (1 or 3)\n');

x_max = 10; % Maximum value for x
x_min = 0;  % Minimum value for x

% Calculate number of nodes, number of cells and the middle node from grid resolution
node_number = (x_max - x_min) / delta_x + 1;
cell_number = node_number - 1;
middle_node = (node_number + 1) / 2;

% Equally spaced node coordinates
x = linspace(x_min, x_max, node_number);

% Define variables
rho = zeros(node_number, 0);    % Densities
u   = zeros(node_number, 0);    % Velocities
p   = zeros(node_number, 0);    % Pressures
E   = zeros(node_number, 0);    % Energies

% Create initial conditions
for i = 1: middle_node                      % Left side of the membrane
    rho(i)  = 1.;
    p(i)    = 1.;
    u(i)    = 0.;
end

for i = middle_node + 1: node_number        % Right side of the membrane
    rho(i)  = 0.125;
    p(i)    = 0.1;
    u(i)    = 0.;
end

E = p / (gamma - 1) + 0.5 * rho .* u .* u;  % Initial energy values

% Matrix containing state variables
Q       = zeros(3, node_number);        % Old values
Q_L     = zeros(3, node_number);        % 
Q_R     = zeros(3, node_number);        % 
F       = zeros(3, node_number);        % Flux values 
F_L     = zeros(3, node_number);        % Flux values from the left side
F_R     = zeros(3, node_number);        % Flux values from the right side
Q_new   = zeros(3, node_number);        % New values
Q(1,:)  = rho;                          % Density
Q(2,:)  = rho.*u;                       % Momentum
Q(3,:)  = E;                            % Energy

delta_T = 0.001;    % Steps in time in seconds
T_max   = 1.2;      % Maximum time value in seconds
T       = 0;        % Initial time value in seconds

Q_new = Q;

while T < T_max
    
    for i = 1: node_number - 1
      
        if(i == 1 || i == 2 || i == node_number - 2  || i == node_number - 1)
            % Phi should be zero (first order approximation)
            Q_L(:, i) = Q(:, i);
            Q_R(:, i) = Q(:, i + 1);
        else
            kappa = 1./3.;                      % Kappa value for 3rd order approximation
            
            u_i   = Q(2, i) / Q(1, i);          % Velocity
            u_L = Q(2, i - 1) / Q(1, i - 1);    % Velocity from the left cell
            u_R = Q(2, i + 1) / Q(1, i + 1);    % Velocity from the right cell
            r   = (u_i - u_L) / (u_R - u_i);    % Ratio of successive gradients to be used in the limiter
            
            % Minmod
            if(order==3)
                Phi = max(0, min(1, r));
            else
                Phi = 0;
            end
            % MUSCL interpolation
            Q_L(:, i) = Q(:, i) ...
                + Phi / 4. * ((1 - kappa) * (Q(:, i) - Q(:, i - 1))...
                +             (1 + kappa) * (Q(:, i + 1) - Q(:, i)));
            Q_R(:, i) = Q(:, i + 1) ... 
                - Phi / 4. * ((1 + kappa) * (Q(:, i + 1) - Q(:, i))...
                +             (1 - kappa) * (Q(:, i + 2) - Q(:, i + 1)));
        end
        

        % Calculate state variables at the cell edges
        rho_L = Q_L(1, i);                                          % Densities
        rho_R = Q_R(1, i);          
        U_L = Q_L(2, i) / Q_L(1, i);                                % Velocities
        U_R = Q_R(2, i) / Q_R(1, i);
        E_L = Q_L(3, i);                                            % Energies
        E_R = Q_R(3, i);
        p_L = (gamma - 1) * (E_L - (1 / 2) * rho_L * U_L * U_L);    % Pressures
        p_R = (gamma - 1) * (E_R - (1 / 2) * rho_R * U_R * U_R); 
        H_L = (E_L + p_L) / rho_L;                                  % Enthalpies
        H_R = (E_R + p_R) / rho_R;  

        % Roe averages
        rho_a   = sqrt(rho_L * rho_R);
        U_a     = (sqrt(rho_L) * U_L + sqrt(rho_R) * U_R) / (sqrt(rho_L) + sqrt(rho_R));
        H_a     = (sqrt(rho_L) * H_L + sqrt(rho_R) * H_R) / (sqrt(rho_L) + sqrt(rho_R));
        c_s_a   = sqrt((gamma - 1) * (H_a - 0.5 * U_a * U_a));  

        % Flux values from the left side
        F_L(1, i) = rho_L * U_L;
        F_L(2, i) = rho_L * U_L * U_L + p_L; 
        F_L(3, i) = (E_L + p_L) * U_L; 


        % Flux values from the right side
        F_R(1, i) = rho_R * U_R;
        F_R(2, i) = rho_R * U_R * U_R + p_R; 
        F_R(3, i) = (E_R + p_R) * U_R; 

        % Eigenvalues from SVD
        LAMBDA = sparse(3, 3);
        LAMBDA(1,1) = U_a;
        LAMBDA(2,2) = U_a + c_s_a;
        LAMBDA(3,3) = U_a - c_s_a;

        % Right eigenvector from SVD
        T_eigen       = zeros(3, 3);
        T_eigen(1, :) = 1;
        T_eigen(2, 1) = LAMBDA(1, 1);
        T_eigen(2, 2) = LAMBDA(2, 2);
        T_eigen(2, 3) = LAMBDA(3, 3);
        T_eigen(3, 1) = 0.5 * U_a * U_a;
        T_eigen(3, 2) = H_a + c_s_a * U_a;
        T_eigen(3, 3) = H_a - c_s_a * U_a;

        % Change in transformed variables
        delta_p        = p_R - p_L;                 % difference in pressure
        delta_Momentum = U_R * rho_R - U_L * rho_L; % difference in momentum
        delta_rho      = rho_R - rho_L;             % difference in density
        delta_W        = zeros(3, 1);
        delta_W(1, 1)     = -1 * delta_p / (2 * c_s_a * c_s_a) + delta_rho;
        delta_W(2, 1)     = delta_p / (2 * c_s_a * c_s_a) + 1 /(2 * c_s_a)...
            * (delta_Momentum - U_a * delta_rho);
        delta_W(3, 1)     = delta_p / (2 * c_s_a * c_s_a) - 1 /(2 * c_s_a)...
            * (delta_Momentum - U_a * delta_rho);
        
        % Compute flux 
        F(:, i) = 0.5 * (F_R(:, i) + F_L(:, i) - T_eigen * abs(LAMBDA) * delta_W);
    end
    
    % Explicit
    for i=2: node_number - 1
        Q_new(:,i) = Q(:,i) - (F(:, i) - F(:, i-1)) * (delta_T / delta_x);
    end
    
    Q = Q_new; % Old = new values
    
    T = T + delta_T;    % Increase time
end

% Analytical solution *************************

% Region 1 is undisturbed (initial conditions)
x_mid = (x_max - x_min) / 2;
rho1 = 1.;
P1   = 1.;
u1   = 0.;
a1   = sqrt(gamma * P1 / rho1);

% Region 5 is undisturbed (initial conditions)
rho5 = 0.125;
P5   = 0.1;
u5   = 0.;

% Pressure in region 3 can be found iteratively
P3 = 1;
P3_new = 0;
error = 1;
m = sqrt((gamma - 1) / (gamma + 1));
parameter1 = sqrt(((1 - m^4)*P1^(1./gamma))/(m^4 * rho1));
parameter3 = (gamma - 1)/(2*gamma);
while(error > 1e-6)
    parameter2 = sqrt((1 - m^2)/(rho5 * (P3 + m^2 * P5)));
    P3_new = (parameter1 * (P1^parameter3 - P3^parameter3)) / parameter2 + P5;
    error = abs(P3_new - P3);
    P3 = P3_new;
end

% Region 4
P4 = P3;
u4 = (P4 - P5) * sqrt((1-m^2) / (rho5 * (P4 + m^2 * P5)));
u3 = u4; % no flow between region 3 and 4
rho4 = rho5 * (P4 + m^2 * P5) / (P5 + m^2 * P4);
rho3 = rho1 * (P3 / P1)^(1./gamma);
a3 = sqrt(gamma * P3 / rho3);

u_shock = u4 * (rho4 / (rho4 - rho5));
x_shock = x_mid + u_shock * T_max;
x_c     = x_mid + u4 * T_max;
x2_3 = x_mid + (u3 / (1 - m^2) - a1) * T_max;
x1_2 = x_mid - 1 * a1 * T_max;

u2   = zeros(node_number, 1);
rho2 = zeros(node_number, 1);
P2   = zeros(node_number, 1);
E2   = zeros(node_number, 1);
for i=1:node_number
    u2(i) = (1 - m^2) * ((x(i) - x_mid) / T_max + a1);
    rho2(i) = (rho1^gamma / (gamma * P1) * (u2(i) - (x(i) - x_mid) / T_max)^2)^(1./(gamma - 1));
    P2(i) = (P1 / rho1^gamma) * rho2(i)^gamma;
end
E2 = P2 / (gamma - 1) + 0.5 * rho2 .* u2 .* u2;
E1 = P1 / (gamma - 1) + 0.5 * rho1 * u1 * u1;
E3 = P3 / (gamma - 1) + 0.5 * rho3 * u3 * u3;
E4 = P4 / (gamma - 1) + 0.5 * rho4 * u4 * u4;
E5 = P5 / (gamma - 1) + 0.5 * rho5 * u5 * u5;

rho_analytical = zeros(node_number, 1);
u_analytical   = zeros(node_number, 1);
P_analytical   = zeros(node_number, 1);
E_analytical   = zeros(node_number, 1);
for i=1:node_number
    if(x(i) <= x1_2)
        rho_analytical(i) = rho1;
        u_analytical(i) = u1;
        P_analytical(i) = P1;
        E_analytical(i) = E1;
    end
    if(x(i) > x1_2 && x(i) <= x2_3)
        rho_analytical(i) = rho2(i);
        u_analytical(i) = u2(i);
        P_analytical(i) = P2(i);
        E_analytical(i) = E2(i);
    end     
    if(x(i) > x2_3 && x(i) <= x_c)
        rho_analytical(i) = rho3;
        u_analytical(i) = u3;
        P_analytical(i) = P3;
        E_analytical(i) = E3;
    end
    if(x(i) > x_c && x(i) <= x_shock)
        rho_analytical(i) = rho4;
        u_analytical(i) = u4;
        P_analytical(i) = P4;
        E_analytical(i) = E4;
    end
    if(x(i) > x_shock)
        rho_analytical(i) = rho5;
        u_analytical(i) = u5;
        P_analytical(i) = P5;
        E_analytical(i) = E5;
    end
end
%**********************************************

Density  = Q(1,:);                   
Velocity = Q(2,:) ./ Q(1,:);
Energy   = Q(3,:);
Pressure = (gamma - 1) * (Energy - 0.5 * Density .* Velocity .* Velocity);

figure;
scatter(x, Density, 10);
hold on;
plot(x, rho_analytical);
hold off;
legend('Roe Method','Analytical');
str = sprintf('Density with grid resolution = %f', delta_x);
title(str);

figure;
scatter(x, Velocity, 10);
hold on;
plot(x, u_analytical);
hold off;
legend('Roe Method','Analytical');
str = sprintf('Velocity with grid resolution = %f', delta_x);
title(str);

figure;
scatter(x, Pressure, 10);
hold on;
plot(x, P_analytical);
hold off;
legend('Roe Method','Analytical');
str = sprintf('Pressure with grid resolution = %f', delta_x);
title(str);


figure;
scatter(x, Energy, 10);
hold on;
plot(x, E_analytical);
hold off;
legend('Roe Method','Analytical');
str = sprintf('Energy with grid resolution = %f', delta_x);
title(str);